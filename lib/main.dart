import 'dart:async';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:live_kh_flutter/ui/pages/main_interface.dart';

Future main() async {
  //TODO: Finish implementing device info printout
  DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
  var deviceInfo;
  String os;
  try {
    deviceInfo = await deviceInfoPlugin.androidInfo;
    AndroidDeviceInfo androidDeviceInfo = deviceInfo;
    debugPrint("Device: ${androidDeviceInfo.model}");
    os = "Android";
    String buildVersion = androidDeviceInfo.version.toString();
    print(androidDeviceInfo.version.toString());
  } on Exception {
    deviceInfo = await deviceInfoPlugin.iosInfo;
    debugPrint("Device: ${deviceInfo.model}");
    print('Running on ${deviceInfo.utsname.machine}');
    os = "iOS";
  }

  runApp(
    MaterialApp(
      title: "Live Kingdom Hall",
      debugShowCheckedModeBanner: false,
      theme: originalLiveKHTheme,
      home: MainInterface(),
    ),
  );
}

ThemeData blueAndBeigeTheme = ThemeData(
    primaryColor: Colors.blueAccent[700],
    primaryColorLight: Colors.deepPurple[50],
    primaryColorDark: Colors.deepPurple[800],
    accentColor: Colors.amberAccent[200],
    canvasColor: Colors.white,
    scaffoldBackgroundColor: const Color(0xEEFFFFFF),
    errorColor: Colors.red,
    primaryIconTheme: IconThemeData(color: Colors.white),
    accentIconTheme: IconThemeData(color: Colors.amberAccent[300]),
    textTheme: TextTheme(button: TextStyle(color: Colors.white)),
    accentTextTheme: TextTheme(body2: TextStyle(color: Colors.black)));

ThemeData originalLiveKHTheme = ThemeData(
    primaryColor: Colors.white, accentColor: Color.fromRGBO(0, 203, 252, 1.0)
  //primaryColor:Color.fromRGBO(0, 203, 252, 1.0)
);

ThemeData purpleTheme = new ThemeData(
    primaryColor: Colors.deepPurple,
    primaryColorLight: Colors.deepPurple[50],
    primaryColorDark: Colors.deepPurple[800],
    accentColor: Colors.deepPurpleAccent,
    canvasColor: Colors.white,
    scaffoldBackgroundColor: const Color(0xEEFFFFFF),
    errorColor: Colors.red,
    primaryIconTheme: IconThemeData(color: Colors.white),
    textTheme: TextTheme(button: TextStyle(color: Colors.white)),
    accentTextTheme: TextTheme(body2: TextStyle(color: Colors.black)));
