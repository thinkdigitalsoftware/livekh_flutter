import 'package:flutter/material.dart';
import 'package:live_kh_flutter/blocs/preferences_bloc.dart';

class PreferencesBlocProvider extends InheritedWidget {
  final PreferencesBloc preferencesBloc = PreferencesBloc();
  final Widget child;

  PreferencesBlocProvider({@required this.child});

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => (oldWidget != this);

  static PreferencesBloc of(BuildContext context) {
    assert(context != null);
    final PreferencesBlocProvider provider =
        context.inheritFromWidgetOfExactType(PreferencesBlocProvider);

    if (provider != null) return provider.preferencesBloc;
    throw new FlutterError(
        'PreferencesBlocProvider.of() called with a context that does not contain a PreferencesBlocProvider.\n'
        'No PreferencesBlocProvider ancestor could be found starting from the context that was passed '
        'to PreferencesBlocProvider.of(). This can happen '
        'if the context you use comes from a widget above those widgets.\n'
        'The context used was:\n'
        '  $context');
  }
}
