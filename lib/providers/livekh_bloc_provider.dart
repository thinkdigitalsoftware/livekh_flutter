import 'package:flutter/material.dart';
import 'package:live_kh_flutter/blocs/live_kh_bloc.dart';

class LiveKHBlocProvider extends InheritedWidget {
  final LiveKHBloc liveKHBloc = LiveKHBloc();

  LiveKHBlocProvider({Key key, Widget child}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static LiveKHBloc of(BuildContext context) {
    assert(context != null);
    final LiveKHBlocProvider provider =
        context.inheritFromWidgetOfExactType(LiveKHBlocProvider);

    if (provider != null) return provider.liveKHBloc;
    throw new FlutterError(
        'LiveKHBlocProvider.of() called with a context that does not contain a LiveKHBlocProvider.\n'
        'No LiveKHBlocProvider ancestor could be found starting from the context that was passed '
        'to LiveKHBlocProvider.of(). This can happen '
        'if the context you use comes from a widget above those widgets.\n'
        'The context used was:\n'
        '  $context');
  }
}
