import 'dart:async';

import 'package:audioplayer/audioplayer.dart';
import 'package:flutter/material.dart';
import 'package:live_kh_flutter/blocs/live_kh_bloc.dart';

class LiveStreamTab extends StatefulWidget {
  final LiveKHBloc model;
  final String imgSrc;
  final String congregationName;

  LiveStreamTab(
      {Key key,
      @required this.model,
      @required this.imgSrc,
      @required this.congregationName})
      : assert(model != null);

  @override
  LiveStreamTabState createState() => LiveStreamTabState();
}

class LiveStreamTabState extends State<LiveStreamTab> {
  final int audioPlayerIndex = 0;
  IconData playPauseIcon;
  double playerBarHeight;
  double audioPosition;
  double audioDuration = 0.0;

  bool get isActive => this.mounted;

  LiveKHBloc get model => widget.model;

  String get streamUrl => model.streamUrl;

  AudioPlayer get audioPlayer => model.audioRegistry.audioPlayer;
  @override
  void initState() {
    assert(this.model != null);
    debugPrint("_AudioTabState.initState() called.");
    //debugPrint("from initState: model.audioRegistry[audioPlayerIndex]: ${model.audioRegistry[audioPlayerIndex]}");
    debugPrint("StreamUrl: $streamUrl");
    audioPosition = 0.0;
    setDuration(model);
    //getPosition(model);
    audioStateListener(model);

    super.initState();
  }

  @override
  void dispose() {
    debugPrint("_AudioTabState.dispose() called.");
    model.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    debugPrint("Widget Build called for LiveStream");
    double height, width;

    return Container();
  }

  void togglePlayBack() {
    debugPrint("liveStream.togglePlayBack running.");
    if (audioPlayer.state != AudioPlayerState.PLAYING) {
      debugPrint("Play command sending.");
      audioPlayer.play(streamUrl);
    } else {
      debugPrint("From LiveStream.togglePlayBack: Pausing");
      audioPlayer.pause();
    }
  }

  String trimTime(String timeString) {
    return timeString.substring(0, timeString.lastIndexOf('.'));
  }

  Duration setDuration(LiveKHBloc model) {
    audioPlayer.onPlayerStateChanged.listen((onData) {
      if (audioPlayer.state == AudioPlayerState.PLAYING) {
        assert(audioPlayer != null);
        audioDuration = audioPlayer.duration.inSeconds.toDouble();
        debugPrint(
            "Duration for audio at index $audioPlayerIndex: $audioDuration");
      }
    });
    return new Duration(seconds: 0);
  }

  /// alerts the slider of the current audio position
  Future getPosition(LiveKHBloc model) async {
    await for (var position in audioPlayer.onAudioPositionChanged) {
      debugPrint(trimTime("$position"));
      if (this.isActive) {
        setState(() {
          audioPosition = position.inSeconds.roundToDouble();
        });
      }
    }
  }

  ///start listening for changes in the audioPlayerState
  Future audioStateListener(LiveKHBloc model) async {
    var onPlayerStateChanged =
        model.audioRegistry.audioPlayer.onPlayerStateChanged;
    try {
      await for (var state in onPlayerStateChanged) {
        debugPrint("AudioPlayer state changed to $state");
        if (state == AudioPlayerState.COMPLETED) {
          model.audioRegistry.stop();
        }
      }
    } catch (e) {
      debugPrint(
          "!\n\n\n\n\n\n\n\nERRRRRRRRRRRRRROR Playing the stream!\n\n\n\n\n\n\n\n");
    }
  }
}
