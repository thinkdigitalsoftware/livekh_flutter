import 'package:audioplayer/audioplayer.dart';
import 'package:flutter/material.dart';
import 'package:live_kh_flutter/blocs/live_kh_bloc.dart';
import 'package:live_kh_flutter/recording.dart';
import 'package:live_kh_flutter/ui/widgets/recording_card.dart';

class RecordingsTab extends StatefulWidget {
  final LiveKHBloc model;

  const RecordingsTab({
    Key key,
    @required this.model,
  }) : super(key: key);

  @override
  _RecordingsTabState createState() => _RecordingsTabState();
}

class _RecordingsTabState extends State<RecordingsTab> {
  int selectedRecordingsIndex; // 0 indexed, not 1 indexed.
  Recording get selectedRecording => recordings[selectedRecordingsIndex];

  LiveKHBloc get model => widget.model;

  List<Recording> get recordings => model.recordings;

  AudioPlayerState playerState;
  AudioPlayer audioPlayer;

  Map<int, AudioPlayerState> cardPlayingStatus = {};

  @override
  void initState() {
    super.initState();
    if (recordings.length >= 1) {
      selectedRecordingsIndex = 0;
    }
    audioPlayer = model.audioRegistry.audioPlayer;
  }

  @override
  void dispose() {
    model.pause();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    bool isActive = this.mounted;
    return Column(
      children: <Widget>[
        Expanded(
          child: ListView.builder(
              itemCount: recordings.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onLongPress: () => debugPrint("Card $index has been held!"),
                  child: new RecordingCard(
                      //adding 1 here so it can register in the AudioRegistry with the correct indices.
                      index: index + 1,
                      recording: recordings[index]),
                );
              }),
        ),
        /*RecordingPlayer(
          index: selectedRecordingsIndex,
          recording: selectedRecording,
          model: model,
        ),*/
      ],
    );
  }

  /// waits for the length of the audio file then sets it for the slider
  Duration setDuration(int index) {
    bool isPlaying = model.audioRegistry.get(index) == AudioPlayerState.PLAYING;
    Duration length = new Duration(seconds: 0);
    if (isPlaying) {
      length = audioPlayer.duration;
    }
    return length;
  }
}
