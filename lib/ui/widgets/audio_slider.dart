import 'dart:async';

import 'package:audioplayer/audioplayer.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:live_kh_flutter/blocs/live_kh_bloc.dart';
import 'package:live_kh_flutter/providers/livekh_bloc_provider.dart';

class AudioSlider extends StatefulWidget {
  @override
  _AudioSliderState createState() => _AudioSliderState();
}

class _AudioSliderState extends State<AudioSlider> {
  bool _isPlaying;

  double audioPosition;

  double audioLength = 0.0;

  bool _listeningToAudioState = false;

  bool _listeningToAudioPosition = false;

  bool _changing = false;

  StreamController audioPositionStream = StreamController<double>();

  @override
  void initState() {
    audioPosition = 0.0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    LiveKHBloc liveKHBloc = LiveKHBlocProvider.of(context);

    /*if (!_listeningToAudioState) {
      audioStateListener(liveKHBloc);
    }
    if (!_listeningToAudioPosition) {
      audioPositionListener(liveKHBloc);
    }*/

    Color textColor = Theme
        .of(context)
        .primaryTextTheme
        .display1
        .color;
    return StreamBuilder(
      stream: liveKHBloc.audioPosition
          .distinct((oldVal, newVal) => oldVal.inSeconds == newVal.inSeconds),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        bool _isPlaying = snapshot.hasData;
        if (snapshot.hasData) {
          audioPosition = (snapshot.data as Duration).inSeconds.toDouble();
        }

        String currentPositionAsString, currentPositionAsStringTrimmed;
        if (audioPosition != null) {
          currentPositionAsString =
              Duration(seconds: audioPosition.toInt()).toString();
          currentPositionAsStringTrimmed = trimTime(currentPositionAsString);
        }

        audioLength = liveKHBloc.audioRegistry.audioPlayer.duration != null
            ? liveKHBloc.audioRegistry.audioPlayer.duration.inSeconds.toDouble()
            : 0.0;
        return Row(
          children: <Widget>[
            Text(
              _isPlaying ? currentPositionAsStringTrimmed : "0:00",
              style: TextStyle(
                  color: Theme
                      .of(context)
                      .primaryTextTheme
                      .display1
                      .color),
            ),
            Expanded(
              child: StreamBuilder(
                  stream: audioPositionStream.stream,
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    return Slider(
                      activeColor: textColor,
                      inactiveColor: textColor.withOpacity(.40),
                      value: (snapshot.hasData &&
                          audioLength != 0.0 &&
                          sliderValuesAreValid(audioPosition, 0.0, audioLength))
                          ? snapshot.data
                          : 0.0,
                      min: 0.0,
                      max: (_isPlaying) ? audioLength : 0.0,
                      divisions: 100,
                      onChangeStart: (double position) {
                        _changing = true;
                      },
                      onChanged: _isPlaying
                          ? (double position) {
                        debugPrintSynchronously("audioPosition = $position");
                        addToAudioPositionStream(position);
                      }
                          : null,
                      onChangeEnd: (double position) {
                        debugPrintSynchronously(
                            "Slider position: $audioPosition");
                        liveKHBloc.seek(position);
                        _changing = false;
                      },
                    );
                  }
              ),
            ),
            Text(trimTime("${Duration(seconds: audioLength.toInt())}"),
                style: TextStyle(color: textColor))
          ],
        );
      },
    );
  }

  bool sliderValuesAreValid(double value, double min, double max) {
    if (value != null &&
        min != null &&
        max != null &&
        min <= max &&
        value >= min &&
        value <= max)
      return true;
    else
      return false;
  }

  String trimTime(String timeString) {
    return timeString.substring(0, timeString.lastIndexOf('.'));
  }

  void audioStateListener(LiveKHBloc model) {
    _listeningToAudioState = true;
    AudioPlayer audioPlayer = model.audioRegistry.audioPlayer;
    audioPlayer.onPlayerStateChanged.listen((state) {
      if (state == AudioPlayerState.PLAYING && this.mounted) {
        setState(() {
          audioLength = audioPlayer.duration.inSeconds.toDouble();
        });
      }
    });
  }

  void audioPositionListener(LiveKHBloc model) {
    _listeningToAudioPosition = true;
    model.audioRegistry.audioPlayer.onAudioPositionChanged.listen((position) {
      if (!_changing && this.mounted) {
        setState(() {
          audioPosition = position.inSeconds.toDouble();
        });
      }
    });
  }

  void addToAudioPositionStream(double position) {
    audioPositionStream.add(position);
  }
}
