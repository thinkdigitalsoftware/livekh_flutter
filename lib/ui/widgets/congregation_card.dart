import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:html/dom.dart' as dom;
import 'package:html/parser.dart' as parser;
import 'package:http/http.dart' as http;
import 'package:live_kh_flutter/congregation.dart';
import 'package:live_kh_flutter/ui/pages/login_page.dart';
import 'package:live_kh_flutter/ui/widgets/congregation_image.dart';

import '../../live_kh.dart';

class CongregationCard extends StatefulWidget {
  final String congregationName;
  final String midweekMeeting;

  final String weekendMeeting;
  final String imgSrc;
  final String congregationUrl;
  final LiveKH liveKH;
  final isLive;

  CongregationCard(
      {@required this.congregationName,
      @required this.midweekMeeting,
      @required this.weekendMeeting,
      @required this.imgSrc,
      @required this.congregationUrl,
        @required this.liveKH,
        this.isLive = false});

  CongregationCard.fromCongregation(Congregation congregation,
      {@required this.liveKH})
      : this.congregationName = congregation.name,
        this.midweekMeeting = congregation.midweekMeeting,
        this.weekendMeeting = congregation.weekendMeeting,
        this.imgSrc = congregation.imgSrc,
        this.congregationUrl = congregation.url,
        this.isLive = congregation.isLive;

  @override
  _CongregationCardState createState() => _CongregationCardState();

  @override
  String toString({DiagnosticLevel minLevel: DiagnosticLevel.debug}) {
    return """
      
      Congregation name: ${this.congregationName}
      Midweek Meeting: ${this.midweekMeeting}
      Weekend Meeting ${this.weekendMeeting}
      Image Source: ${this.imgSrc}
      url: ${this.congregationUrl}
      isLive: ${this.isLive}
      """;
  }

  ///Converts the CongregationCard's data to json and then writes it a file
  ///called recent in the documentsDirectory
  toJson() async {
    Map<String, String> congregation = {
      "congregation_name": this.congregationName,
      "midweek_meeting": this.midweekMeeting,
      "weekend_meeting": this.weekendMeeting,
      "image_source": this.imgSrc,
      "url": this.congregationUrl,
      "isLive": this.isLive,
    };

    return jsonEncode(congregation);
  }

  Congregation toCongregation() =>
      Congregation(
          name: this.congregationName,
          midweekMeeting: this.midweekMeeting,
          weekendMeeting: this.weekendMeeting,
          imgSrc: this.imgSrc,
          url: this.congregationUrl,
          isLive: this.isLive);
}

class _CongregationCardState extends State<CongregationCard> {
  double imageSize = 100.0;
  String id;
  Color inkWellHighLightColor;

  Function get toCongregation => widget.toCongregation;

  LiveKH get liveKH => widget.liveKH;

  @override
  Widget build(BuildContext context) {
    if (widget.congregationUrl == null) {
      inkWellHighLightColor = Theme
          .of(context)
          .errorColor;
    } else {
      inkWellHighLightColor = Theme
          .of(context)
          .primaryColorLight;
    }

    /*double screenHeight = MediaQuery
        .of(context)
        .size
        .height;
    double screenWidth = MediaQuery
        .of(context)
        .size
        .width;*/

    return new InkWell(
      highlightColor: inkWellHighLightColor,
      splashColor: Colors.blue,
      child: Card(
        color: Theme
            .of(context)
            .canvasColor,
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  //image
                  Hero(
                    child: CongregationImage(
                      imgSrc: widget.imgSrc,
                      size: imageSize,
                    ),
                    tag: widget.congregationName,
                  ),
                  Padding(padding: EdgeInsets.symmetric(horizontal: 10.0)),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(right: 15.0),
                      width: 220.0,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          new Text(
                            widget.congregationName,
                            softWrap: true,
                            //overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.center,
                            style: new TextStyle(fontWeight: FontWeight.bold),
                          ),
                          new Divider(),
                          new Column(
                            children: <Widget>[
                              new Text(
                                "Midweek Meeting:",
                                style:
                                new TextStyle(fontWeight: FontWeight.bold),
                              ),
                              new Text("${widget.midweekMeeting}")
                            ],
                          ),
                          new Divider(),
                          new Column(
                            children: <Widget>[
                              new Text(
                                "Weekend Meeting:",
                                style:
                                new TextStyle(fontWeight: FontWeight.bold),
                              ),
                              new Text("${widget.weekendMeeting}")
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              right: 10.0,
              bottom: 10.0,
              child: widget.isLive
                  ? Icon(
                Icons.hearing,
                color: Theme
                    .of(context)
                    .iconTheme
                    .color,
              )
                  : Container(),
            )
          ],
        ),
      ),
      onTap: () async {
        debugPrint("Congregation ID: $id");
        if (widget.congregationUrl != null) {
          await Navigator
              .of(context)
              .push(new CupertinoPageRoute(builder: (BuildContext context) {
            return new LoginPage(congregation: toCongregation(),
              congregationId: id,
              liveKH: liveKH,);
          })).then((onValue) => widget.liveKH.logout());
        } else {
          Scaffold.of(context).removeCurrentSnackBar();
          Scaffold.of(context).showSnackBar(
              SnackBar(content: Text("This congregation has no login page")));
        }
      },
    );
  }

  Future<String> getId() async {
    if (widget.congregationUrl == null) {
      debugPrint("url is null, therefore ID will be empty");
      return "";
    } else {
      debugPrint(widget.congregationUrl);
      http.Response response = await http.get(widget.congregationUrl);
      String body = response.body;
      List<dom.Element> boxSer =
          parser.parse(body).getElementsByClassName("box-ser");
      if (boxSer.length > 0) {
        List<dom.Element> inputs = boxSer[0].getElementsByTagName("input");
        if (inputs.length > 0) {
          if (inputs[0].attributes['name'] == "congregation_id") {
            String id = inputs[0].attributes['value'];
            debugPrint("Congregation ID: $id");
            this.id = id;
            return id;
          }
        }
      }
      throw Exception(
          "Url is present, but ID cannot be found. Please check this.");
    }
  }

  @override
  void initState() {
    assert(widget.congregationName != null);
    assert(widget.imgSrc != null);
    this.getId();
    super.initState();
  }
}
