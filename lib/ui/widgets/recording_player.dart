import 'dart:async';
import 'dart:io';

import 'package:audioplayer/audioplayer.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:live_kh_flutter/blocs/live_kh_bloc.dart';
import 'package:live_kh_flutter/recording.dart';
import 'package:live_kh_flutter/utils.dart';
import 'package:path_provider/path_provider.dart';

class RecordingPlayer extends StatefulWidget {
  final int index;
  final Recording recording;
  final LiveKHBloc model;
  final streamUrl;
  final isLiveStream;

  const RecordingPlayer(
      {Key key,
      @required this.index,
      @required this.recording,
      @required this.model,
      this.streamUrl})
      : isLiveStream = false,
        super(key: key);

  RecordingPlayer.liveStream({this.index = 0, @required this.model})
      : this.streamUrl = model.streamUrl,
        isLiveStream = true,
        this.recording = new Recording(
            title: "Live", date: "$getCurrentDate", sourceUrl: model.streamUrl);

  static get getCurrentDate =>
      new DateFormat.yMd().format(DateTime.now().toLocal());

  @override
  _RecordingPlayerState createState() => _RecordingPlayerState();
}

class _RecordingPlayerState extends State<RecordingPlayer> {
  double audioPosition;
  bool isPlaying;
  Icon playBackIcon;
  Icon selectionIcon;

  LiveKHBloc get model => widget.model;
  AudioPlayer audioPlayer;
  double audioLength = 0.0;

  int get index => widget.index;

  bool get isActive => this.mounted;
  bool gpRunning = false;
  bool isAudioStateListenerRunning = false;

  Recording get recording => widget.recording;

  String get streamUrl => widget.recording.sourceUrl;

  String get getCurrentTime =>
      new DateFormat.jms().format(DateTime.now().toLocal());

  Map<String, Icon> icons;

  final int tabIndex = 3; // referring to the TabBarView in HallPage

  @override
  void initState() {
    super.initState();
    audioPosition = 0.0;
    isPlaying ??= false;
    audioPlayer = model.audioRegistry.audioPlayer;
  }

  @override
  dispose() {
    super.dispose();
  }

  ///start listening for changes in the audioPlayerState
  Future audioStateListener(LiveKHBloc model) async {
    if (!isAudioStateListenerRunning) {
      isAudioStateListenerRunning = true;
      String statement;
      String oldStatement;

      audioPlayer.onPlayerStateChanged.listen((state) {
        isPlaying = model.audioRegistry.currentlyPlaying == index;
        if (isPlaying) {
          if (oldStatement != statement) {
            debugPrintSynchronously(
                "AudioPlayer state for index $index changed to $state");
          }

          oldStatement = statement;

          if (isActive) {
            if (state == AudioPlayerState.COMPLETED) {
              model.audioRegistry.stop();
            }

            if (state == AudioPlayerState.PLAYING) {
              debugPrintSynchronously(
                  "From RecordingCard.audioStateListener: Setting playBackIcon to pause.");
              /*setState(() {
                //playBackIcon = icons["pause"];
              });*/
            } else {
              debugPrintSynchronously(
                  "From RecordingCard.audioStateListener: Setting playBackIcon to play.");
              /*setState(() {
                //playBackIcon = icons["play"];
              });*/
            }
          }
        }
      });
    }
  }

  Future currentlyPlayingListener(LiveKHBloc model) async {
    print("AudioRegister: Listening to index $index for AudioState changes.");
    var statement;
    var oldStatement;

    await for (var currentlyPlayingIndex
        in model.audioRegistry.currentlyPlayingStream) {
      if (currentlyPlayingIndex == index) {
        this.isPlaying = true;
        debugPrintSynchronously("RecordingCard $index is now playing");
        setState(() {
          null;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    String currentPositionAsString =
        Duration(seconds: audioPosition.toInt()).toString();
    String currentPositionAsStringTrimmed = trimTime(currentPositionAsString);

    icons = {
      "play": Icon(
        Icons.play_circle_filled,
        color: Theme.of(context).primaryColor,
        size: 35.0,
      ),
      "pause": Icon(
        Icons.pause_circle_outline,
        color: Theme.of(context).primaryColor,
        size: 35.0,
      ),
      "active": Icon(
        Icons.check,
        color: Colors.green,
        size: 25.0,
      ),
      "inactive": Icon(
        Icons.blur_circular,
        size: 25.0,
      )
    };
    //for setDuration to work
    audioLength = setDuration(model).inSeconds.toDouble();

    getPosition(model);
    audioStateListener(model);
    currentlyPlayingListener(model);

    double elevation = 3.0;
    return Card(
      elevation: elevation,
      child: ListTile(
        /*leading: IconButton(
          icon: !isPlaying ? icons["play"] : icons["pause"],
          onPressed: () => togglePlayBack(model),
        ),*/
        title: Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: RichText(
            textAlign: TextAlign.justify,
            text: TextSpan(
              text: "${
                  recording.
                  title} - ",
              style: DefaultTextStyle
                  .of(context)
                  .style
                  .copyWith(fontWeight: FontWeight.bold),
              children: <TextSpan>[
                TextSpan(
                    text: recording.date,
                    style: DefaultTextStyle
                        .of(context)
                        .style
                        .copyWith(fontStyle: FontStyle.italic))
              ],
            ),
          ),
        ),
        subtitle: Row(
          children: <Widget>[
            Text(currentPositionAsStringTrimmed),
            Expanded(
              child: Slider(
                value: (isPlaying &&
                        audioLength != 0.0 &&
                        sliderValuesAreValid(audioPosition, 0.0, audioLength))
                    ? (widget.isLiveStream ? getCurrentTime : audioPosition)
                    : 0.0,
                min: 0.0,
                max: (isPlaying)
                    ? (widget.isLiveStream ? getCurrentTime : audioLength)
                    : 0.0,
                divisions: 100,
                onChanged: isPlaying
                    ? (double position) {
                        setState(() {
                          audioPosition = position.floorToDouble();
                        });
                      }
                    : null,
                onChangeEnd: (double position) {
                  debugPrintSynchronously("Slider position: $audioPosition");
                  audioPlayer.seek(position);
                },
              ),
            ),
            Text(trimTime("${Duration(seconds: audioLength.toInt())}"))
          ],
        ),
        //Text("${recordings[index].title} -"),
      ),
    );
  }

  void cacheAudioFile() async {
    final tempDir = await getTemporaryDirectory();
    String fileName = streamUrl.substring(streamUrl.lastIndexOf('/') + 1);
    final File audioFile = File("$tempDir/$fileName");
    debugPrintSynchronously("Caching file...");
    var client = new HttpClient();
    var request = await client.getUrl(Uri.parse(streamUrl));
    var response = await request.close();
    var audioBytes = await consolidateHttpClientResponseBytes(response);
    await audioFile.writeAsBytes(audioBytes);
    debugPrintSynchronously("${'__' * 30} \n ${tempDir.list()}");
  }

// stored here so we can access the AudioPlayer and AudioPlayerState
  /// alerts the slider of the current audio position
  Future getPosition(LiveKHBloc model) async {
    if (!gpRunning) {
      gpRunning = true;
      Duration oldPosition;
      await for (var position in audioPlayer.onAudioPositionChanged) {
        isPlaying = model.audioRegistry.get(index) == AudioPlayerState.PLAYING;
        String statement;
        if (isPlaying) {
          statement = "From RecordingCard.getPosition: Index $index: ${trimTime(
              "$position")}";
          if (oldPosition != position) {
            debugPrintSynchronously(statement);
            oldPosition = position;
            if (isActive) {
              setState(() {
                audioPosition = position.inSeconds.roundToDouble();
              });
            }
          }
        }
      }
    }
  }

  /// waits for the length of the audio file then sets it for the slider
  Duration setDuration(LiveKHBloc model) {
    bool isPlaying = model.audioRegistry.get(index) == AudioPlayerState.PLAYING;
    Duration length = new Duration(seconds: 0);
    if (isPlaying) {
      length = audioPlayer.duration;
    }
    return length;
  }

  void togglePlayBack(LiveKHBloc model) {
    if (!isPlaying) {
      Scaffold.of(context).removeCurrentSnackBar();
      Scaffold.of(context).showSnackBar(
          SnackBar(content: Text("This Card is not playing. Playing")));
      // check to see if this audio file needs to be resumed or stop and play a new one.
      if (model.audioRegistry.get(index) == AudioPlayerState.STOPPED) {
        model.audioRegistry
            .stop(); // stop current stream before starting a new one.
      }
      model.audioRegistry.play(fromIndex: index, url: streamUrl); // play it
      setState(() {
        playBackIcon = icons['pause'];
      });
    } else {
      Scaffold.of(context).removeCurrentSnackBar();
      Scaffold.of(context).showSnackBar(
          SnackBar(content: Text("This Card is playing. Pausing")));
      model.audioRegistry.pause(fromIndex: index);
      setState(() {
        playBackIcon = icons['play'];
      });
    }
  }

  bool sliderValuesAreValid(double value, double min, double max) {
    if (value != null &&
        min != null &&
        max != null &&
        min <= max &&
        value >= min &&
        value <= max)
      return true;
    else
      return false;
  }

  @override
  String toString({DiagnosticLevel minLevel: DiagnosticLevel.debug}) {
    return """
      Card info:
      Index: ${this.index}
      Recording: ${this.recording}
      """;
  }
}
