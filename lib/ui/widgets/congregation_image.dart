import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class CongregationImage extends StatelessWidget {
  const CongregationImage({Key key,
    @required this.imgSrc,
    this.isLoading = false,
    @required this.size})
      : super(key: key);

  final String imgSrc;
  final isLoading;
  final double size;

  @override
  Widget build(BuildContext context) {
    assert(isLoading != null);

    double animationSize = size / 3.5;
    var animationPosition = size * (38 / 100);
    return Stack(alignment: Alignment.center, children: [
      Card(
        elevation: 4.0,
        shape: CircleBorder(),
        child: Image.network(
          imgSrc,
          fit: BoxFit.fitHeight,
          width: size,
          height: size,
        ),
      ),
      isLoading
          ? Stack(
        children: <Widget>[
          Opacity(
            child: Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: Colors.black),
              width: size,
              height: size,
            ),
            opacity: 0.25,
          ),
          Positioned(
            left: animationPosition, //size / 2,
            top: animationPosition,
            child: SpinKitFadingCube(
              color: Colors.white,
              size: animationSize,
            ),
          ),
        ],
      )
          : new Container(
        width: 0.0,
        height: 0.0,
      ),
    ]);
  }
}
