import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TimeText extends StatefulWidget {
  @override
  _TimeTextState createState() => _TimeTextState();
}

class _TimeTextState extends State<TimeText> {
  String currentTime;

  String get _getCurrentTime =>
      new DateFormat.jms().format(DateTime.now().toLocal());

  @override
  void initState() {
    updateTime();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Text(_getCurrentTime);
  }

  Future updateTime() async {
    while (this.mounted) {
      setState(() {
        currentTime = _getCurrentTime;
      });
      await Future.delayed(Duration(seconds: 1));
    }
  }
}
