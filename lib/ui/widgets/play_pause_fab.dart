import 'dart:async';

import 'package:audioplayer/audioplayer.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:live_kh_flutter/audio_registry.dart';
import 'package:live_kh_flutter/blocs/live_kh_bloc.dart';
import 'package:live_kh_flutter/providers/livekh_bloc_provider.dart';

class PlayPauseFloatingActionButton extends StatefulWidget {
  @override
  _PlayPauseFloatingActionButtonState createState() =>
      _PlayPauseFloatingActionButtonState();
}

class _PlayPauseFloatingActionButtonState
    extends State<PlayPauseFloatingActionButton> {
  Icon _floatingActionIcon;

  @override
  void initState() {
    super.initState();
  }

  LiveKHBloc liveKHBloc;

  @override
  Widget build(BuildContext context) {
    liveKHBloc = LiveKHBlocProvider.of(context);

    Map<String, Icon> icons = {
      "play": Icon(
        Icons.play_arrow,
        color: Theme.of(context).primaryIconTheme.color,
        size: 35.0,
      ),
      "pause": Icon(
        Icons.pause,
        size: 35.0,
      )
    };

    return StreamBuilder<AudioPlayerState>(
        stream: liveKHBloc.audioPlayerState,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          bool _isPlaying = snapshot.data == AudioPlayerState.PLAYING;
          // if the recordingCard is empty and the playing is not playing, then we're not playing
          _floatingActionIcon = _isPlaying ? icons["pause"] : icons["play"];

          Future _floatingActionButtonOnPressed() async {
            // ignore: missing_enum_constant_in_switch
            switch (liveKHBloc.activeTab) {
              case KHTab.LIVESTREAM:
                {
                  if (_isPlaying == false) {
                    //play
                    if (liveKHBloc.lastPlayingFromIndex != KHTab.LIVESTREAM) {
                      await liveKHBloc.stop();
                    }
                    if (liveKHBloc.streamUrl != null) {
                      await liveKHBloc.play(
                          fromIndex: 0, url: liveKHBloc.streamUrl);
                      liveKHBloc.lastPlayingFromIndex = KHTab.LIVESTREAM;
                    }
                  } else if (_isPlaying == true) {
                    await liveKHBloc.pause();
                  }
                }
                break;

              case KHTab.VIDEOPLAYER:
                {
                  if (liveKHBloc.lastPlayingFromIndex != KHTab.VIDEOPLAYER) {
                    liveKHBloc.stop();
                  }
                  liveKHBloc.lastPlayingFromIndex = KHTab.VIDEOPLAYER;
                }
                break;

              case KHTab.RECORDINGSTAB:
                {
                  if (_isPlaying == false) {
                    // if paused or stopped,
                    ActiveRecordingCard activeRecordingCard =
                        liveKHBloc.getActiveRecordingCard;

                    bool cardIsValid = activeRecordingCard != null &&
                        activeRecordingCard.isNotEmpty;

                    if (cardIsValid) {
                      //TODO: ADD LOADING
                      if (liveKHBloc.lastPlayingFromIndex !=
                          KHTab.RECORDINGSTAB) {
                        liveKHBloc.stop();
                      }

                      await liveKHBloc.play(
                          byActiveRecordingCard: activeRecordingCard);
                      liveKHBloc.lastPlayingFromIndex = KHTab.RECORDINGSTAB;
                    } else {
                      Scaffold.of(context).removeCurrentSnackBar();
                      Scaffold.of(context).showSnackBar(SnackBar(
                          content: Text("Please select a recording first.")));
                    }
                  } else if (_isPlaying) {
                    // if currently playing something
                    await liveKHBloc.pause();
                  }
                }
                break;
            } // switch
          } // _floatingActionButtonOnPressed

          _floatingActionIcon = _isPlaying ? icons["pause"] : icons["play"];

          return FloatingActionButton(
            tooltip: _isPlaying ? "Pause" : "Play",
            //backgroundColor: Colors.black38,//Theme.of(context).primaryColor,
            child: _floatingActionIcon,
            onPressed: _floatingActionButtonOnPressed,
            elevation: 6.0,
            //foregroundColor: Theme.of(context).accentColor,
          );
        });
  }

  Future<bool> exists(String url) async {
    //url = "http://tunein.streamguys1.com/cnn?aw_0_1st.playerid=RadioTime&aw_0_1st.skey=1531534561";
    var headResult = await http.head(url);
    var headStatusCode = headResult.statusCode;

    return headStatusCode >= 200 && headStatusCode < 300;

    /*working stream HTML
    <html class="gr__audio_livekingdomhall_com"><head><meta name="viewport" content="width=device-width"></head><body data-gr-c-s-loaded="true" crossrider_data_store_temp="{}"><video controls="" autoplay="" name="media"><source src="http://audio.livekingdomhall.com:29166/stream" type="audio/mpeg"></video></body></html>*/
  }
}
