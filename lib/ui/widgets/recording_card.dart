import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:live_kh_flutter/blocs/live_kh_bloc.dart';
import 'package:live_kh_flutter/providers/livekh_bloc_provider.dart';
import 'package:live_kh_flutter/recording.dart';

enum Elevation { ENABLED, DISABLED }

class SBar extends StatelessWidget {
  final Widget content;

  final SnackBarAction action;

  const SBar({Key key, @required this.content, this.action}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SnackBar(
      content: content,
      action: action,
    );
  }
}

class RecordingCard extends StatefulWidget {
  final Recording recording;
  final int index;

  const RecordingCard({Key key, this.recording, this.index}) : super(key: key);

  @override
  RecordingCardState createState() {
    return new RecordingCardState();
  }
}

class RecordingCardState extends State<RecordingCard> {
  bool _isActive = false;
  bool _checkBoxValue = false;
  Color textColor;
  get index => widget.index;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    LiveKHBloc liveKHBloc = LiveKHBlocProvider.of(context);
    _isActive = liveKHBloc.getActiveRecordingCard.index == index;
    double elevation = _isActive ? 4.0 : 2.0;
    _checkBoxValue = _isActive;
    textColor = _isActive ? Colors.black : Colors.grey;

    void _onTap() {
      _isActive ??= false;
      assert(_isActive != null);

      if (!_isActive) {
        //do stuff to activate the card and update the model.
        liveKHBloc.setActiveRecordingCard(index, widget.recording.sourceUrl);
      } else {
        //do stuff to deactivate the card and update the model.
        liveKHBloc.deactivateRecordingCard(index);
      }
      assert(_isActive == _checkBoxValue);
      setState(() {
        _isActive = liveKHBloc.getActiveRecordingCard?.index == index;
      });
      debugPrint("Card $index _isActive = $_isActive");

    }

    return InkWell(
      onTap: _onTap,
      child: Card(
        elevation: elevation,
        child: ListTile(
          /*leading: Checkbox(activeColor: Theme.of(context).accentColor,
                value: _checkBoxValue,
                onChanged: (bool value) => _onTap()),*/
            title: Center(
              child: RichText(
                textAlign: TextAlign.justify,
                text: TextSpan(
                  text: "${widget.recording.title} - ",
                  style: DefaultTextStyle
                      .of(context)
                      .style
                      .copyWith(fontWeight: FontWeight.bold, color: textColor),
                  children: <TextSpan>[
                    TextSpan(
                        text: widget.recording.date,
                        style: DefaultTextStyle
                            .of(context)
                            .style
                            .copyWith(
                            fontStyle: FontStyle.italic, color: textColor))
                  ],
                ),
              ),
            ),
            subtitle: null
          //Text("${recordings[index].title} -"),
        ),
      ),
    );
  }
}
