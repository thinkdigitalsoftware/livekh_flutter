import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:live_kh_flutter/congregation.dart';
import 'package:live_kh_flutter/live_kh.dart';
import 'package:live_kh_flutter/providers/livekh_bloc_provider.dart';
import 'package:live_kh_flutter/ui/pages/hall_page.dart';
import 'package:live_kh_flutter/ui/widgets/congregation_image.dart';
import 'package:scoped_model/scoped_model.dart';

class LoginPage extends StatefulWidget {
  final String congregationId;
  final String congregationUrl;
  final String imgSrc;
  final LiveKH liveKH;
  final Congregation congregation;

  LoginPage(
      {Key key,
        @required this.congregation,
      @required this.congregationId,
        @required this.liveKH})
      : assert(congregationId != null),
        congregationUrl = congregation.url,
        imgSrc = congregation.imgSrc,
        super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _scaffoldStateKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  String _listPass;
  int dropDownButtonValue = 1;

  bool _isLoading = false;
  Model model;

  LiveKH get liveKH => widget.liveKH;

  Congregation get congregation => widget.congregation;

  @override
  void initState() {
    debugPrint("Logging into ${congregation.name}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    Text numOfListenersText = new Text("Select the number of listeners: ");

    return Scaffold(
        resizeToAvoidBottomPadding: false,
        key: _scaffoldStateKey,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        appBar: AppBar(
          title: Text("Login"),
          centerTitle: true,
        ),
        body: Center(child:
            OrientationBuilder(builder: (BuildContext context, orientation) {
          double cardWidth;

          double cardHeight;

          Widget textFormField = TextFormField(
              validator: (String _listPass) {
                return _listPass != null && _listPass.length >= 1
                    ? null
                    : "Please Enter a Valid Password";
              },
              maxLength: 4,
              decoration: InputDecoration(
                hintText: "Enter Password",
              ),
              obscureText: true,
              onSaved: (String listPass) {
                _listPass = listPass;
              },
              onFieldSubmitted: (String value) {
                if (_formKey.currentState.validate() && !_isLoading) {
                  _formKey.currentState.save();
                  _listPass = value;
                  login(
                      listPass: _listPass, numOfListeners: dropDownButtonValue);
                } else
                  return null;
              });

          var loginButton = RaisedButton(
              disabledColor: Color.fromRGBO(155, 136, 185, 1.0),
              color: Theme.of(context).primaryColor,
              child: Text(
                "Login",
                style:
                    TextStyle(color: Theme.of(context).textTheme.button.color),
              ),
              onPressed: () {
                if (_formKey.currentState.validate() && !_isLoading) {
                  _formKey.currentState.save();
                  login(
                      listPass: _listPass, numOfListeners: dropDownButtonValue);
                } else
                  return null;
              });

          if (orientation == Orientation.portrait) {
            cardWidth = screenWidth;
            cardHeight = screenHeight / 1.75;
            return Card(
              color: Theme.of(context).canvasColor,
              elevation: 10.0,
              child: Container(
                width: cardWidth,
                height: cardHeight,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Hero(
                        tag: congregation.name,
                        child: new CongregationImage(
                          imgSrc: widget.imgSrc,
                          isLoading: _isLoading,
                          size: cardWidth / 3,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10.0),
                      ),
                      Text(
                        "${congregation.name}",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.w200, fontSize: 20.0),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8.0, horizontal: 30.0),
                        child: Form(
                            key: _formKey,
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  textFormField,
                                  Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: screenWidth / 10)),
                                  Row(
                                    children: <Widget>[
                                      numOfListenersText,
                                      Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 10.0)),
                                      DropdownButton<int>(
                                        value: dropDownButtonValue,
                                        isDense: true,
                                        items: new List<
                                                DropdownMenuItem<int>>.generate(
                                            10,
                                            (i) => DropdownMenuItem<int>(
                                                  value: i,
                                                  child: Text("$i"),
                                                )),
                                        onChanged: (int value) {
                                          setState(() {
                                            dropDownButtonValue = value;
                                          });
                                        },
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(10.0),
                                  ),
                                  loginButton
                                ])),
                      ),
                    ]),
              ),
            );
          } else

            ///
            /// Landscape Orientation Design Starts Below
            ///  {
            cardWidth = screenWidth / 1.4;
          cardHeight = screenHeight / 1.2;
          return Card(
            color: Theme.of(context).canvasColor,
            elevation: 10.0,
            child: Container(
              margin: EdgeInsets.all(30.0),
              width: cardWidth,
              height: cardHeight,
              child: ListView(
                children: <Widget>[
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: <
                      Widget>[
                    Hero(
                      tag: congregation.name,
                      child: new CongregationImage(
                        imgSrc: widget.imgSrc,
                        isLoading: _isLoading,
                        size: cardHeight / 2.5,
                      ),
                    ),
                    Padding(padding: EdgeInsets.symmetric(horizontal: 20.0)),
                    // 2nd Column
                    Expanded(
                      child: Form(
                        key: _formKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "${congregation.name}",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontWeight: FontWeight.w200, fontSize: 20.0),
                            ),
                            textFormField,
                            Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: screenWidth / 10)),
                            Row(
                              children: <Widget>[
                                numOfListenersText,
                                Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 10.0)),
                                DropdownButton<int>(
                                  value: dropDownButtonValue,
                                  isDense: true,
                                  items:
                                      new List<DropdownMenuItem<int>>.generate(
                                          10,
                                          (i) => DropdownMenuItem<int>(
                                                value: i,
                                                child: Text("$i"),
                                              )),
                                  onChanged: (int value) {
                                    setState(() {
                                      dropDownButtonValue = value;
                                    });
                                  },
                                ),
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.all(10.0),
                            ),
                            Center(child: loginButton)
                          ],
                        ),
                      ),
                    )
                  ]),
                ],
              ),
            ),
          );
        })));
  }

  Future login(
      {@required String listPass, @required int numOfListeners}) async {
    debugPrint('Login Button Pressed.');
    setState(() {
      _isLoading = true;
    });
    assert(congregation.name != null);
    assert(widget.congregationUrl != null);
    assert(widget.congregationId != null);
    assert(listPass != null);
    assert(numOfListeners != null);
    debugPrint("Assertions Passed");
    debugPrint("ListPass: $listPass");
    debugPrint("NumOfListeners: $numOfListeners");

    Map loginResult = await widget.liveKH.login(
        congregationId: widget.congregationId,
        congregationName: congregation.name,
        congregationUrl: widget.congregationUrl,
        listPass: listPass);

    if (loginResult['status']) {
      liveKH.addToRecent(congregation);
      String body = loginResult['response']?.body;
      setState(() {
        _isLoading = false;
      });

      await Navigator
          .of(context)
          .push(new CupertinoPageRoute(builder: (BuildContext context) {
        return LiveKHBlocProvider(
          child: HallPage(
            congregationName: congregation.name,
            body: body,
            imgSrc: congregation.imgSrc,
          ),
        );
      })).whenComplete(() => liveKH.logout());
    } else {
      showErrorSnackBar();
      setState(() {
        _isLoading = false;
      });
    }
  }

  void showErrorSnackBar() {
    SnackBar snackBar = SnackBar(
        content:
            Text("Login failed. Please check your password and try again"));
    _scaffoldStateKey.currentState.removeCurrentSnackBar();
    _scaffoldStateKey.currentState.showSnackBar(snackBar);
  }
}
