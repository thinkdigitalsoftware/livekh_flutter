import 'dart:async';

import 'package:flutter/material.dart';
import 'package:live_kh_flutter/blocs/preferences_bloc.dart';
import 'package:live_kh_flutter/congregation.dart';
import 'package:live_kh_flutter/live_kh.dart';
import 'package:live_kh_flutter/providers/preferences_bloc_provider.dart';
import 'package:live_kh_flutter/ui/pages/congregation_grid_view.dart';
import 'package:live_kh_flutter/ui/search.dart' as search;
import 'package:flutter/foundation.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:live_kh_flutter/ui/widgets/congregation_card.dart';

class MainInterface extends StatefulWidget {
  const MainInterface({Key key}) : super(key: key);

  @override
  MainInterfaceState createState() {
    return MainInterfaceState();
  }
}

class MainInterfaceState extends State<MainInterface> {
  final categories = [
    "Nearest Kingdom Halls",
    "U.S.A.",
    "North / South America",
    "Europe / Asia",
    "Africa / Australia / Other",
    "Recent"
  ];

  LiveKH liveKH = LiveKH();

  _LiveKHSearchDelegate _delegate;

  TextEditingController searchController = TextEditingController();
  String selectedCategory;
  Widget appBarTitleWidget = Text("Live Kingdom Hall");

  Map<String, List<Congregation>> congregations;

  String _lastCongregationSelected;

  Widget mainBody;

  bool _loading = true;

  @override
  Widget build(BuildContext context) {
    selectedCategory ??= categories[0];
    if (_loading) {
      _loadCongregations();
      mainBody = Center(
        child: SpinKitFadingCube(
          color: Theme.of(context).accentColor,
          size: 50.0,
        ),
      );
    } else {
      mainBody = CongregationGridView(
        congregationList: congregations,
        selectedCategory: selectedCategory,
        liveKH: liveKH,
      );
    }
    _delegate = _LiveKHSearchDelegate(liveKH: liveKH);

    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: AppBar(
        title: Text("Live Kingdom Hall"),
        centerTitle: true,
        // change this to avoid copyright infringement.
        actions: <Widget>[
          IconButton(
            tooltip: 'Search',
            icon: const Icon(Icons.search),
            onPressed: () async {
              final String selected = await search.showSearch<String>(
                context: context,
                delegate: _delegate,
              );
              if (selected != null && selected != _lastCongregationSelected) {
                setState(() {
                  _lastCongregationSelected = selected;
                });
              }
            },
          ),
        ],
      ),
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            //disabling header for now until accounts are implemented.
            /*UserAccountsDrawerHeader(
              decoration: BoxDecoration(color: Theme.of(context).primaryColor),
              accountEmail: Text("Email"),
              accountName: Text("Username"),
            ),*/
            DrawerHeader(child: Text("")),

            //Drawer Tiles
            Expanded(
              child: ListView.builder(
                  itemCount: categories.length,
                  shrinkWrap: true,
                  padding: EdgeInsets.zero,
                  itemBuilder: (BuildContext context, int selectedIndex) {
                    var selectedCategory = categories[selectedIndex];
                    return Column(
                      children: <Widget>[
                        ListTile(
                            title: Text(selectedCategory),
                            onTap: () {
                              debugPrint(
                                  "Index $selectedIndex of Drawer tapped!");
                              Navigator.pop(context);
                              /*if (selectedCategory != "Recent") {*/
                              _loadCongregations(
                                  selectedCategory: selectedCategory);
                              /* } else {
                              setState(() {
                                mainBody = CongregationGridView(
                                  congregationList: congregations,
                                  selectedCategory: selectedCategory,
                                  liveKH: liveKH,
                                  child: preferencesBloc.recents.length > 0
                                      ? ListView.builder(
                                          itemCount:
                                              preferencesBloc.recents.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return CongregationCard
                                                .fromCongregation(
                                              preferencesBloc.recents[index],
                                              liveKH: liveKH,
                                            );
                                          })
                                      : Center(child: Text("No recents")),*/
                            }),
                        Divider(
                          height: 0.1,
                          color: Colors.black45,
                        )
                      ],
                    );
                  }),
            )
          ],
        ),
      ),
      body: RefreshIndicator(
        child: mainBody,
        onRefresh: _loadCongregations,
      ),
    );
  }

  Future<Null> _loadCongregations({String selectedCategory}) async {
    this._loading = true;
    Map<String, List<Congregation>> congregations =
        await liveKH.getCongregations();
    selectedCategory ??= this.selectedCategory;
    setState(() {
      this._loading = false;
      this.congregations = congregations;
      this.selectedCategory = selectedCategory;
    });
  }
}

class _LiveKHSearchDelegate extends search.SearchDelegate<String> {
  final LiveKH liveKH;

  List<String> _history = <String>[];

  _LiveKHSearchDelegate({@required this.liveKH});

  @override
  ThemeData appBarTheme(BuildContext context) {
    assert(context != null);
    final ThemeData theme = Theme.of(context);
    assert(theme != null);
    return theme.copyWith(
      primaryIconTheme: theme.primaryIconTheme,
      primaryColorBrightness: Brightness.light,
      primaryTextTheme: theme.textTheme.copyWith(
        display1: TextStyle(color: Colors.white),
        display2: TextStyle(color: Colors.white),
        display3: TextStyle(color: Colors.white),
        display4: TextStyle(color: Colors.white),
      ),
    );
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      /*query.isEmpty
          ?  IconButton(
              tooltip: 'Voice Search',
              icon: const Icon(Icons.mic),
              onPressed: () {
                query = '';
              },
            )
          : */
      IconButton(
        tooltip: 'Clear',
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
          showSuggestions(context);
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      tooltip: 'Back',
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    final String congregationName = query;
    return CongregationGridView.fromFuture(
      congregationFuture: liveKH.search(congregationName),
      selectedCategory: "default",
      liveKH: liveKH,
    );
    //TODO: Implement
    /*
      children: <Widget>[
         _ResultCard(
          title: 'This integer',
          integer: searched,
          searchDelegate: this,
        ),
         _ResultCard(
          title: 'Next integer',
          integer: searched + 1,
          searchDelegate: this,
        ),
         _ResultCard(
          title: 'Previous integer',
          integer: searched - 1,
          searchDelegate: this,
        ),
      ],
    */
  }

  @override
  Widget buildSuggestions(BuildContext context) {
//    return currentView;
    final Iterable<String> suggestions = _history;
    return _SuggestionList(
      query: query,
      suggestions: suggestions.map((String i) => '$i').toList(),
      onSelected: (String suggestion) {
        query = suggestion;
        showResults(context);
      },
    );
  }
}

class _SuggestionList extends StatelessWidget {
  const _SuggestionList({this.suggestions, this.query, this.onSelected});

  final List<String> suggestions;
  final String query;
  final ValueChanged<String> onSelected;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return ListView.builder(
      itemCount: suggestions.length,
      itemBuilder: (BuildContext context, int i) {
        final String suggestion = suggestions[i];
        return ListTile(
          leading: query.isEmpty ? const Icon(Icons.history) : const Icon(null),
          title: RichText(
            text: TextSpan(
              text: suggestion.substring(0, query.length),
              style:
                  theme.textTheme.subhead.copyWith(fontWeight: FontWeight.bold),
              children: <TextSpan>[
                TextSpan(
                  text: suggestion.substring(query.length),
                  style: theme.textTheme.subhead,
                ),
              ],
            ),
          ),
          onTap: () {
            onSelected(suggestion);
          },
        );
      },
    );
  }
}
