import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:html/dom.dart' as dom;
import 'package:html/parser.dart';
import 'package:live_kh_flutter/blocs/live_kh_bloc.dart';
import 'package:live_kh_flutter/providers/livekh_bloc_provider.dart';
import 'package:live_kh_flutter/recording.dart';
import 'package:live_kh_flutter/ui/tabs/live_stream.dart';
import 'package:live_kh_flutter/ui/tabs/recordings_tab.dart';
import 'package:live_kh_flutter/ui/tabs/video_player_tab.dart';
import 'package:live_kh_flutter/ui/widgets/audio_slider.dart';
import 'package:live_kh_flutter/ui/widgets/play_pause_fab.dart';

///inspired by http://papermashup.com/demos/javascript-ui-audio-player/

enum KHTab { LIVESTREAM, VIDEOPLAYER, RECORDINGSTAB, NULL }

class HallPage extends StatefulWidget {
  final String congregationName;
  final String body;
  final String imgSrc;

  HallPage(
      {Key key,
      @required this.congregationName,
      @required this.body,
      @required this.imgSrc})
      : assert(body != null,
            "HallPage has been initalized without a valid body String.");

  @override
  HallPageState createState() => HallPageState();
}

class HallPageState extends State<HallPage> with TickerProviderStateMixin {
  bool _isPlaying;

  double audioPosition;

  @override
  void initState() {
    super.initState();
    _isPlaying = false;
  }

  @override
  Widget build(BuildContext context) {
    LiveKHBloc liveKHBloc = LiveKHBlocProvider.of(context);
    assert(liveKHBloc != null);

    dom.Document document = parse(widget.body);

    List<dom.Element> audioSettingDiv =
    document.getElementsByClassName("audio-setting");
    dom.Element recordingsDiv;
    Map<String, String> recordingsMap = {};

    if (audioSettingDiv.length > 2) {
      recordingsDiv = audioSettingDiv[2];

      for (var i = 0; i < recordingsDiv.nodes.length; i++) {
        if (recordingsDiv.nodes[i].text.contains("Meeting")) {
          recordingsMap[recordingsDiv.nodes[i].text.trim()] =
              recordingsDiv.nodes[i + 2].text.trim().replaceFirst("Date- ", "");
        }
      }

      liveKHBloc.sources = document.getElementsByTagName("source");
      Iterable<dom.Element> where = liveKHBloc.sources.where(
              (dom.Element element) =>
              element.attributes['src'].endsWith("stream"));
      liveKHBloc.streamUrl =
      where.length > 0 ? where
          .elementAt(0)
          .attributes['src'] : null;

      //recordings
      List<dom.Element> recordingElements = [];
      for (var element in liveKHBloc.sources) {
        if (element.attributes['src'].endsWith(".mp3")) {
          recordingElements.add(element);
        }
      }

      List<String> sourceUrls = [];
      for (var element in recordingElements) {
        sourceUrls.add(element.attributes['src']);
      }
      liveKHBloc.recordings = convertToRecording(recordingsMap, sourceUrls);
    } else {
      liveKHBloc.recordings = [];
    }
    //tabs
    liveKHBloc.tabs = [
      LiveStreamTab(
        model: liveKHBloc,
        imgSrc: widget.imgSrc,
        congregationName: widget.congregationName,
      ),
      VideoPlayerTab(),
      RecordingsTab(
        model: liveKHBloc,
      ),
    ];
    liveKHBloc.tabController =
        TabController(length: liveKHBloc.tabs.length, vsync: this);

    var screenWidth = MediaQuery
        .of(context)
        .size
        .width;
    return Scaffold(
        backgroundColor: Theme
            .of(context)
            .scaffoldBackgroundColor,
        appBar: AppBar(
          title: Text(
            widget.congregationName,
            overflow: TextOverflow.ellipsis,
          ),
          centerTitle: true,
          bottom: TabBar(
              indicatorColor: Theme
                  .of(context)
                  .accentColor,
              controller: liveKHBloc.tabController,
              tabs: [
                Tab(
                  text: "Live",
                  icon: Icon(Icons.people),
                ),
                Tab(
                  text: "Video",
                  icon: Icon(Icons.video_library),
                ),
                Tab(
                  text: "Recordings",
                  icon: Icon(Icons.mic),
                ),
              ]),
        ),
        body: TabBarView(
          children: liveKHBloc.tabs,
          controller: liveKHBloc.tabController,
        ),
        floatingActionButton: PlayPauseFloatingActionButton(),
        floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
        bottomNavigationBar: BottomAppBar(
          color: Theme
              .of(context)
              .primaryColor,
          child: SizedBox(
            height: 50.0,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(padding: EdgeInsets.only(right: 40.0)),
                Expanded(child: AudioSlider()),
                Padding(
                  padding: const EdgeInsets.only(left: 100.0),
                )
              ],
            ),
          ),
        ));
  }

  List<Recording> convertToRecording(
      Map<String, String> recordingsMap, List<String> sourceUrls) {
    List<Recording> recordings = [];
    assert(recordingsMap.length ==
        sourceUrls.length); // Make sure something fishy isn't going on.
    for (var i = 0; i < recordingsMap.length; i++) {
      String entry = recordingsMap.keys.toList()[i];
      recordings.add(new Recording(
          title: entry, date: recordingsMap[entry], sourceUrl: sourceUrls[i]));
    }
    return recordings;
  }

  var audioRegistry; // for the shutdown procedure.

  @override
  void didChangeDependencies() {
    audioRegistry = LiveKHBlocProvider
        .of(context)
        .audioRegistry;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    audioRegistry.shutDown();
    super.dispose();
  }
}
