import 'dart:async';

import 'package:flutter/material.dart';
import 'package:live_kh_flutter/congregation.dart';
import 'package:live_kh_flutter/live_kh.dart';
import 'package:live_kh_flutter/ui/widgets/congregation_card.dart';

class CongregationGridView extends StatelessWidget {
  final Map<String, List<Congregation>> congregationList;
  final String selectedCategory;
  final LiveKH liveKH;
  final child;
  final Future<Map> congregationFuture;
  final Type paramType;

  CongregationGridView(
      {Key key,
      this.congregationList,
      @required this.selectedCategory,
      @required this.liveKH,
      this.child})
      : assert(congregationList != null || child != null),
        congregationFuture = null,
        paramType = Map,
        super(key: key);

  CongregationGridView.fromFuture({
    Key key,
    this.congregationFuture,
    @required this.selectedCategory,
    @required this.liveKH,
    this.child,
  })  : assert(congregationFuture != null || child != null),
        congregationList = null,
        paramType = Future,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    double childAspectRatio = 1.75;
    bool isLoading;
    int columns;
    //PreferencesBloc preferencesBloc = PreferencesBlocProvider.of(context);
    List<Widget> congregations = [];

    double screenWidth =
        MediaQuery.of(context).size.width; //iPad Pro 768.0, iPhone X 375.0
    double screenHeight = MediaQuery.of(context).size.height;

    debugPrint("Device Screen Width: $screenWidth");
    debugPrint("Device Screen Height: $screenHeight");

    if (child != null) {
      return child;
    }

    if (congregationList != null) {
      //Set up screen layout
      if (screenWidth < 500) {
        columns = 1;
      } else if (screenWidth < 990) {
        columns = 2;
      } else if (screenWidth < 1100) {
        columns = 3;
      } else {
        columns = 4;
      }

      for (Congregation cong in congregationList[selectedCategory]) {
        congregations.add(
          CongregationCard.fromCongregation(
            cong,
            liveKH: liveKH,
          ),
        );
      }
      /*assert(congregationList != null);
      congregations.addAll(
        congregationList[selectedCategory].map(
          (congregation) =>
              CongregationCard.fromCongregation(congregation, liveKH: liveKH),
        ),
      );*/

      isLoading = false;

      return GridView.count(
        shrinkWrap: true,
        padding: EdgeInsets.all(10.0),
        mainAxisSpacing: 5.0,
        crossAxisSpacing: 5.0,
        crossAxisCount: columns,
        childAspectRatio: childAspectRatio,
        children: congregations,
      );
    } else {
      //Future
      assert(congregationFuture != null);
      if (screenWidth < 500) {
        columns = 1;
      } else if (screenWidth < 990) {
        columns = 2;
      } else if (screenWidth < 1100) {
        columns = 3;
      } else {
        columns = 4;
      }

      return FutureBuilder(
          future: congregationFuture,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            switch (snapshot.connectionState) {
              case (ConnectionState.waiting):
                {
                  return Center(child: Text("loading"));
                }
              case (ConnectionState.done):
                {
                  assert(snapshot.hasData);
                  for (Congregation cong in snapshot.data[selectedCategory]) {
                    congregations.add(
                      CongregationCard(
                        congregationName: cong.name,
                        midweekMeeting: cong.midweekMeeting,
                        weekendMeeting: cong.weekendMeeting,
                        imgSrc: cong.imgSrc,
                        congregationUrl: cong.url,
                        liveKH: liveKH,
                        isLive: cong.isLive,
                      ),
                    );
                  }

                  isLoading = false;

                  return GridView.count(
                    shrinkWrap: true,
                    padding: EdgeInsets.all(10.0),
                    mainAxisSpacing: 5.0,
                    crossAxisSpacing: 5.0,
                    crossAxisCount: columns,
                    childAspectRatio: childAspectRatio,
                    children: congregations,
                  );
                }
            }
          });
    }
  }
}
