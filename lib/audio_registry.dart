import 'dart:async';

import 'package:audioplayer/audioplayer.dart';
import 'package:http/http.dart' as http show head;

/// Keeps track of which audio files are playing and coordinates with other files
/// that are currently playing

class AudioRegistry {
  Map<int, AudioPlayerState> _registry = {};
  ActiveRecordingCard _activeRecordingCard = new ActiveRecordingCard();

  ActiveRecordingCard get activeRecordingCard => _activeRecordingCard;

  Stream<AudioPlayerState> get audioPlayerState =>
      audioPlayer.onPlayerStateChanged;

  //StreamControllers
  StreamController<ActiveRecordingCard> activeRecordingCardStreamController =
      new StreamController<ActiveRecordingCard>.broadcast();

  StreamController<int> currentlyPlayingStreamController =
      new StreamController<int>.broadcast();

  AudioPlayer audioPlayer = new AudioPlayer();
  bool isShuttingDown = false;
  String streamUrl;

  int _currentlyPlaying; // ignore: unused_field

  set activeRecordingCard(ActiveRecordingCard activeRecordingCard) {
    _activeRecordingCard = activeRecordingCard;
    activeRecordingCardStreamController.add(activeRecordingCard);

    if (activeRecordingCard != null) {
      stopAllExcept(activeRecordingCard.index);
    }
  }

  AudioRegistry() {
    activeRecordingCardStreamController.stream.listen((card) {
      print("ActiveRecordingCard set to $card");
    });
  }

  int get currentlyPlaying {
    for (var i = 0; i < _registry.length; i++) {
      if (_registry[i] == AudioPlayerState.PLAYING) {
        assert(
            _registry.containsKey(i),
            "Something is wrong. Registry does not contain this key! "
            "Find another way to iterate");
        return i;
      }
    }
    return -1;
  }

  set currentlyPlaying(int currentlyPlayingIndex) {
    _registry[currentlyPlayingIndex] = AudioPlayerState.PLAYING;
    _currentlyPlaying = currentlyPlayingIndex;

    // make sure there's only one item playing
    assert(
        _registry.values
                .where((value) => value == AudioPlayerState.PLAYING)
                .length ==
            1,
        "More than one item is listed as playing!");
  }

  operator [](int index) => get(index);

  void deactivateRecordingCard(int index) {
    _activeRecordingCard = new ActiveRecordingCard();
    print("RecordingCard $index deactivated.");
  }

  Stream<int> get currentlyPlayingStream {
    StreamController<int> controller = new StreamController<int>();
    var oldCurrentlyPlaying = currentlyPlaying;
    new Timer.periodic(Duration(milliseconds: 500), (Timer timer) {
      if (currentlyPlaying != oldCurrentlyPlaying) {
        controller.add(currentlyPlaying);
        print("${"===" *
            5} Currently Playing has changed to $currentlyPlaying! ${"===" *
            5}");
        oldCurrentlyPlaying = currentlyPlaying;
      }
      if (isShuttingDown) {
        timer.cancel();
        controller.close();
      }
    });
    return controller.stream;
  }

  void deregister(index) {
    assert(
        _registry.containsKey(index),
        "This index is not currently in the registry.\n "
        "Please check your indices!");

    _registry.remove(index);
  }

  AudioPlayerState get(int index) {
    if (_registry.containsKey(index)) {
      return _registry[index];
    } else {
      print("===== Adding index $index to the registry. =====");
      _registry[index] = AudioPlayerState.STOPPED;
      audioStateListener(index);
      return _registry[index];
    }
  }

  ///start listening for changes in the audioPlayerState
  Future audioStateListener(int index) async {
    print("AudioRegister: Listening to index $index for AudioState changes.");

    await for (var state in audioPlayer.onPlayerStateChanged) {
      if (state == AudioPlayerState.COMPLETED) {
        audioPlayer.stop();
      }
    }
  }

  Future listener(int index) async {
    print("AudioRegister: Listening to index $index for AudioState changes.");

    await for (var state in audioPlayer.onPlayerStateChanged) {
      if (state == AudioPlayerState.COMPLETED) {
        audioPlayer.stop();
      }
    }
  }

  Future pause({int fromIndex, ActiveRecordingCard activeRecordingCard}) async {
    // ignore: unrelated_type_equality_checks
    activeRecordingCard ??= this.activeRecordingCard;
    if (fromIndex == null && activeRecordingCard != null) {
      fromIndex = activeRecordingCard.index;
    }

    //assert(_registry[fromIndex] == AudioPlayerState.PLAYING,
    //  "This stream wasn't playing. Check your code!");

    Future pause = audioPlayer.pause();

    if (fromIndex != null) {
      //deactivateRecordingCard(fromIndex);
      _registry[fromIndex] = AudioPlayerState.PAUSED;
    }

    _currentlyPlaying = -1;
    return pause;
  }

  Future play(
      {int fromIndex,
      String url,
      ActiveRecordingCard byActiveRecordingCard}) async {
    // ignore: unrelated_type_equality_checks
    assert(fromIndex != byActiveRecordingCard && url != byActiveRecordingCard);
    if (fromIndex == null) {
      assert(byActiveRecordingCard != null);
      fromIndex = byActiveRecordingCard.index;
      url = byActiveRecordingCard.url;
    }

    if (byActiveRecordingCard != null) {
      assert(fromIndex == null && url == null,
          "You may only submit an index and url or an ActiveRecordingCard, but not both");
    }

    assert(url != null);
    if (currentlyPlaying != -1) audioPlayer.stop();
    await audioPlayer.play(url).catchError((error) {
      print("ERROR!!!: $error");
    });

    activeRecordingCard = new ActiveRecordingCard(fromIndex, url);
    stopAllExcept(fromIndex);
    print("From AudioRegistry.play(): index: $fromIndex \nPlaying $url");
  }

  Future exists(String url) async {
    var result = await http.head(url);
    var statusCode = result.statusCode;
    print("From AudioRegistry.exist: $url => $statusCode");

    return statusCode >= 200 && statusCode < 300;
  }

  void shutDown() {
    isShuttingDown = true;
    stop();
    _registry.clear();
    print("Registry Shut Down Sucessfully.");
    activeRecordingCardStreamController.close();
    currentlyPlayingStreamController.close();
  }

  Future stop() => audioPlayer.stop();

  void stopAllExcept(int index) {
    print("Running AudioRegistry.stopAllExcept");
    for (int entry in _registry.keys) {
      if (entry != index) {
        if (_registry[entry] == AudioPlayerState.PLAYING) {
          print("Stopping _archive[$entry]");
          _registry[entry] = AudioPlayerState.STOPPED;
        }
      }
    }
    assert(_registry.values
            .where(
                (AudioPlayerState state) => state == AudioPlayerState.PLAYING)
            .length <
        2);
  }

  @override
  String toString() => _registry.toString();
}

class ActiveRecordingCard {
  int index;
  String url;

  ActiveRecordingCard([this.index, this.url]);

  bool get isNotEmpty => (index != null && url != null);

  bool get isEmpty => (index == null && url == null);

  @override
  String toString() {
    return "Index: $index, Url: $url";
  }
}
