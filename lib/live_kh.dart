import 'dart:async';
import 'dart:convert';

import 'package:flutter/services.dart' show rootBundle;
import 'package:html/dom.dart';
import 'package:html/parser.dart' as htmlParser;
import 'package:http/http.dart' as http;
import 'package:live_kh_flutter/congregation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LiveKH {
  http.Client client;

  final String homePageUrl = "https://www.livekingdomhall.com/";

  Future<String> download(String url, {offline = false}) async {
    String body;
    if (offline) {
      body = await rootBundle.loadString("assets/homepage.html");
    } else {
      http.Response response = await http.get(url);
      body = response.body;
      print("From LiveKH.download: Url:${response.request.url}");
    }

    return body;
  }

  ///Returns a map of congregations under the various categories returned by the homepage.
  ///for the search page, use search
  Future<Map<String, List<Congregation>>> getCongregations() async {
    Document document = htmlParser.parse(
      await download(homePageUrl),
    );
    //all the congregations per category are under this html element.
    List<Element> categoryElements = document.getElementsByClassName("list-bx");

    List categoryNames = [];
    for (Element element in document.getElementsByClassName("cilent_text")) {
      categoryNames.add(element.text.trim().replaceAll(r'\n', ""));
    }

    Map<String, List<Congregation>> categories = {};

    for (int i = 0; i < categoryElements.length; i++) {
      Element element = categoryElements[i];

      //get the list of congregations from this element

      List<Congregation> congregationList = extractCongregations(element);

      categories[categoryNames[i]] = congregationList;
      categories['Recent'] = await getRecent();
    }
    return categories;
  }

  ///Returns a Map of congregations returned by the search page. The key for this map is 'default'.
  Future<Map> search(String congregationName) async {
    Map category = {'default': []};
    String responseBody = await download(
        "https://www.livekingdomhall.com/search-kingdomhall?congregation_name="
        "${congregationName.trim().replaceAll(
            " ", "+")}&congrgation_search=",
        offline: false);

    Document document = htmlParser.parse(responseBody);
    Element element = document.getElementsByTagName("html")[0];
    category['default'] = extractCongregations(element);
    return category;
  }

  /// Logs into the Kingdom Hall and returns a [Map] with a 'success' key with a boolean value
  /// and a 'response' key with an [http.Response] value.
  Future<Map> login(
      {String congregationName,
      String congregationUrl,
      String congregationId,
      String listPass}) async {
    assert(congregationName != null);
    assert(congregationUrl != null);
    assert(congregationId != null);

    congregationName = congregationName.replaceAll(" ", "%20");
    client = new http.Client();
    http.Response loginResultPage;
    String cookie;
    final String authUrl =
        "https://www.livekingdomhall.com/kingdomhall/authenticate";
    var response = await client.get(congregationUrl);
    cookie = response.headers['set-cookie'];

    String body = response.body;
    String xsrf =
        cookie.substring(cookie.indexOf("XSRF-TOKEN="), cookie.indexOf(";"));
    String laravell = cookie.substring(cookie.indexOf("laravel_session="),
        cookie.indexOf(";", cookie.indexOf("laravel_session=")));
    String finalCookie = xsrf + ";" + laravell;

    String token = htmlParser
        .parse(body)
        .getElementById("form_login")
        .getElementsByTagName('input')[0]
        .attributes['value'];

    Map<String, String> header = {
      "referrer": congregationUrl,
      "origin": "https://www.livekingdomhall.com",
      "Content-Type": "application/x-www-form-urlencoded",
      "cookie": finalCookie
    };

    print("CongregationUrl: $congregationUrl");
    print("token: $token");
    print("cookies: $finalCookie");
    print("header: $header");

    http.Response postResponse = await client.post(
      authUrl,
      headers: header,
      body: {
        "_token": "$token",
        "btn_login_listen": "",
        "congregation_id": congregationId,
        "congregation_name": congregationName,
        "list_pass": "$listPass"
      },
    ).catchError((onError) {
      null;
    });
    var body2 = postResponse?.body;
    var document2 = htmlParser.parse(body2);
    bool isLoggedIn =
        document2.getElementsByClassName("text-details").length == 0;

    cookie = postResponse.headers['set-cookie'];
    xsrf = cookie.substring(cookie.indexOf("XSRF-TOKEN="), cookie.indexOf(";"));
    laravell = cookie.substring(cookie.indexOf("laravel_session="),
        cookie.indexOf(";", cookie.indexOf("laravel_session=")));
    String _ga = "GA1.2.98988204.1528957944";
    String _gid = "GA1.2.1241994663.1529362181";
    finalCookie =
        "_ga=$_ga; gsScrollPos-30=; gsScrollPos-22=; _gid=$_gid;$xsrf;$laravell";

    header = {
      "Accept":
          "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
      "Accept-Encoding": "gzip, deflate, br",
      "Accept-Language": "en-US,en;q=0.5",
      "Connection": "keep-alive",
      "Host": "www.livekingdomhall.com",
      "referrer": congregationUrl,
      "origin": "https:www.livekingdomhall.com",
      "Content-Type": "application/x-www-form-urlencoded",
      "cookie": finalCookie
    };

    String loginUrl = getListenUrl(congregationUrl);
//    loginUrl =
//    "https://www.livekingdomhall.com/kingdomhall/listen/South+Spanish+-+Desert+Hot+Springs/NzY3MA==";
    loginResultPage = await client.get(loginUrl, headers: header);
    var body3 = loginResultPage.body;
    var document3 = htmlParser.parse(body3);
    isLoggedIn = document3.getElementsByClassName("text-details").length == 0;
    if (!isLoggedIn) {
      print('${"=" * 50}\n LOGIN FAILED\n${"=" * 50}');
    }

    Map<String, dynamic> result = {
      'status': isLoggedIn,
      'response': loginResultPage
    };
    return result;
  }

  List<Congregation> extractCongregations(Element element) {
    List<Congregation> congregationList = [];

    List<Element> congregationElements =
        element.getElementsByClassName("hall-list");

    for (Element congregation in congregationElements) {
      String congName =
          congregation.getElementsByClassName("tour-head")[0].innerHtml;
      String imgSrc =
          congregation.getElementsByClassName('box-img')[0].attributes['src'];
      List<Element> pars = congregation.getElementsByTagName("p");
      String url = congregation.getElementsByTagName('a')[0].attributes['href'];

      //grabbing info after the colon.
      String midweekMeeting =
          pars[0].text.substring(pars[0].text.indexOf(':') + 1).trim();
      String weekendMeeting =
          pars[1].text.substring(pars[0].text.indexOf(':') + 1).trim();

      var liveLabelBoxList = congregation.querySelectorAll('.live-lable-box');
      bool isLive = liveLabelBoxList.isNotEmpty;

      congregationList.add(Congregation(
        name: congName,
        midweekMeeting: midweekMeeting,
        weekendMeeting: weekendMeeting,
        imgSrc: imgSrc,
        url: url,
        isLive: isLive,
      ));
    }

    return congregationList;
  }

  void setListenerCount(Congregation congregation,
      {String token, int listenerCount}) {
    String listenerCountUrl = "https://www.livekingdomhall.com/setlistnercount";
    String data =
        "_token=$token&rec_id=345741&congregation_id=${congregation
        .id}&congregation_name=${congregation
        .name}&listner_cnt=$listenerCount&btn_set_count=";
    htmlParser.parse(download(congregation.url));
  }

  void logout() {
    //TODO: Add code to log out of the kingdomHall
    print("Logging Out");
    client?.close();
  }

  static String getListenUrl(String congregationUrl) {
    int firstBreak = congregationUrl.indexOf("kingdomhall/") + 12;
    int lastBreak = congregationUrl.lastIndexOf("/") + 1;
    return "${congregationUrl.substring(
        0, firstBreak)}listen/${congregationUrl.substring(
        lastBreak)}/${congregationUrl.substring(firstBreak, lastBreak - 1)}";
  }

  void addToRecent(Congregation congregation) async {
    final sharedPrefs = await SharedPreferences.getInstance();
    String recent = sharedPrefs.getString('recent');
    Map recentMap = json.decode(recent);
    recentMap[congregation.name] = congregation.toJson();
    await sharedPrefs.setString("recent", json.encode(recentMap));
  }

  Future<List<Congregation>> getRecent() async {
    addToRecent(Congregation(name: "test",
        midweekMeeting: "test",
        weekendMeeting: "test",
        imgSrc: "test",
        url: "test",
        isLive: true));
    final sharedPrefs = await SharedPreferences.getInstance();
    String recent = sharedPrefs.getString('recent');
    recent ??= "{}";
    Map recentMap = json.decode(recent);
    return recentMap.values.toList();
  }
}
