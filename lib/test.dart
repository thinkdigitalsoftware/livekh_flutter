import 'package:flutter/material.dart';
import 'package:live_kh_flutter/ui/widgets/congregation_image.dart';

main(List<String> arguments) {
  IconData add = Icons.add;
  Icon icon1 = Icon(add);
  assert(icon1.icon == Icons.add);
  runApp(new MaterialApp(
      title: "Test",
      home: CongregationImage(
        imgSrc:
        "https://www.livekingdomhall.com/public/uploads/congregation_pic/resize_cache/desert-hot-banner_1-210x127.jpg",
        size: 100.0,
      )));
}

