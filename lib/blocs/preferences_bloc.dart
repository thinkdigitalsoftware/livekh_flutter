import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:live_kh_flutter/congregation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:scoped_model/scoped_model.dart';

class PreferencesBloc extends Model {
  PreferencesBloc() {
    _loadRecents();
    debugPrintSynchronously("PreferencesBloc created.");
  }

  List<Congregation> recents = [];

  void addToRecents(Congregation congregation) {
    recents.add(congregation);
    _writeRecents();
  }

  Future _writeRecents() async {
    final directory = await getApplicationDocumentsDirectory();
    final String path = directory.path;
    File recentCongregations = File("$path/recent.json");
    for (Congregation congregation in recents) {
      String encodedCongregation = jsonEncode(congregation.toJson());
      recentCongregations.writeAsString(encodedCongregation);
    }
  }

  void _loadRecents() async {
    final directory = await getApplicationDocumentsDirectory();
    final String path = directory.path;
    File recentCongregationsFile = File("$path/recent.json");
    if (recentCongregationsFile.existsSync()) {
      var congregationsJson =
          json.decode(await recentCongregationsFile.readAsString());
      null;
      // recents.add();
    }
  }

  Congregation congregationFromJson(String json) {
    Map congregation = jsonDecode(json) as Map<String, String>;
    return Congregation.fromJson("");
  }
}
