import 'dart:async';

import 'package:audioplayer/audioplayer.dart';
import 'package:flutter/material.dart';
import 'package:html/dom.dart' as dom;
import 'package:live_kh_flutter/audio_registry.dart';
import 'package:live_kh_flutter/recording.dart';
import 'package:live_kh_flutter/ui/widgets/congregation_card.dart';


enum KHTab { LIVESTREAM, VIDEOPLAYER, RECORDINGSTAB, NULL }

class LiveKHBloc {
// for registering which playing streams are playing
  AudioRegistry audioRegistry = new AudioRegistry();

  TabController tabController;
  List<dom.Element> sources;
  List<Recording> recordings; // for recordingsTab. Initialized in initState.
  List<Widget> tabs;

  List<CongregationCard> recents = [];
  bool congregationGridViewIsLoading;

  KHTab lastPlayingFromIndex;

  LiveKHBloc({Key key});

  KHTab get activeTab {
    int index = this.tabController.index;
    if (index == 0) {
      return KHTab.LIVESTREAM;
    } else if (index == 1) {
      return KHTab.VIDEOPLAYER;
    } else if (index == 2) {
      return KHTab.RECORDINGSTAB;
    } else {
      return KHTab.NULL;
    }
  }

  Stream<AudioPlayerState> get audioPlayerState =>
      audioRegistry.audioPlayerState;

  ActiveRecordingCard get getActiveRecordingCard =>
      audioRegistry.activeRecordingCard;

  get streamUrl => audioRegistry.streamUrl;

  Stream<Duration> get audioPosition =>
      audioRegistry.audioPlayer.onAudioPositionChanged;

  set streamUrl(String streamUrl) => audioRegistry.streamUrl = streamUrl;


  void deactivateRecordingCard(int index) {
    audioRegistry.activeRecordingCard = null;
    print("RecordingCard $index deactivated.");
  }

  Future pause({int fromIndex, ActiveRecordingCard activeRecordingCard}) async {
    int pause = await audioRegistry.pause(
        fromIndex: fromIndex, activeRecordingCard: activeRecordingCard);
    return pause;
  }

  /// Calls audioRegistry.play().
  /// if [byActiveRecordingCard] is defined,
  /// [play] will expand it into the [fromIndex] and [url] parameters.
  Future play({int fromIndex,
    String url,
    ActiveRecordingCard byActiveRecordingCard}) async {
    // ignore: unrelated_type_equality_checks

    if (byActiveRecordingCard != null && byActiveRecordingCard.isNotEmpty) {
      fromIndex = byActiveRecordingCard.index;
      url = byActiveRecordingCard.url;
    }

    assert(url != null);
    int play = await audioRegistry.play(fromIndex: fromIndex, url: url);
    return play;
  }

  void seek(double position) => audioRegistry.audioPlayer.seek(position);

  //recent tab functions
  void setActiveRecordingCard(int index, String url) {
    audioRegistry.activeRecordingCard = new ActiveRecordingCard(index, url);
  }

  Future stop() async => await audioRegistry.stop();


}
