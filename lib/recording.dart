import 'package:meta/meta.dart';

class Recording{
  String _title;
  String _date;

  String _sourceUrl;

  Recording({@required String title, @required String date, String sourceUrl}) {
    this._title = title;
    this._date = date.trim().replaceAll("\n", "");
    this.sourceUrl = sourceUrl;
  }

  String get title => _title;

  set title(String title) => this._title = title?.trim();

  String get date => _date;

  set date(String value) => _date = value?.replaceAll("\n", "")?.trim();

  get sourceUrl => _sourceUrl;

  set sourceUrl(String value) => _sourceUrl = value?.trim();

  @override
  String toString() => "title: $title \ndate: $date, \nsourceUrl: $sourceUrl\n";
}