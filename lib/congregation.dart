import 'dart:convert';

import 'package:meta/meta.dart';

class Congregation {
  String id;
  String name;
  String midweekMeeting;
  String weekendMeeting;
  String imgSrc;
  String url;
  bool isLive;

  Congregation(
      {@required this.name,
      @required this.midweekMeeting,
      @required this.weekendMeeting,
      @required this.imgSrc,
      @required this.url,
      @required this.isLive});

  String toJson() {
    return json.encode({
      "name": name,
      "midweekMeeting": midweekMeeting,
      "weekendMeeting": weekendMeeting,
      "imgSrc": imgSrc,
      "url": url,
      "isLive": isLive,
    });
  }

  Congregation.fromJson(String encodedJson) {
    Map parsedJson = json.decode(encodedJson);

    name = parsedJson['name'];
    midweekMeeting = parsedJson[midweekMeeting];
    weekendMeeting = parsedJson['weekendMeeting'];
    imgSrc = parsedJson['imgSrc'];
    url = parsedJson['url'];
    isLive = parsedJson['isLive'];
  }
}
